#include <iostream>
#include <vector>
#include <queue>
using namespace std;

class Graph
{
protected:
    class Node
    {
    public:
        int label;  // vertex identifier
        int weight; // weight, if any
        Node *next; // next in the list
    };

    Node **neighbors; // adjacency info
    int *degrees;     // outdegree of each vertex
    int nvertices;
    int nedges;
    bool directed;

public:
    Graph(int n, bool directed) : nvertices(n), directed(directed)
    {
        degrees = new int[n + 1];
        neighbors = new Node *[n + 1];
        nedges = 0;

        for (int i = 0; i < n; i++)
        {
            degrees[i] = 0;
            neighbors[i] = NULL;
        }
    }

    void insert_edge(int x, int y, bool directed)
    {
        Node *p = new Node;
        p->weight = 1;
        p->label = y;
        p->next = neighbors[x];
        neighbors[x] = p;
        degrees[x]++;

        if (directed == false)
        {
            insert_edge(y, x, true);
        }
        else
        {
            nedges++;
        }
    }

    friend ostream& operator<<(ostream& out, Graph& g);
};

ostream& operator<<(ostream &out, Graph &g)
{
    for (int i = 1; i <= g.nvertices; i++)
    {
        cout << i << ": ";
        Graph::Node *p = g.neighbors[i];
        while (p != NULL)
        {
            cout << " " << p->label;
            p = p->next;
        }
        cout << "\n";
    }
    return out;
}

int main(void)
{
    int n, m; cin >> n >> m;
    bool directed = false;
    Graph g = Graph(n, directed);

    for (int x, y, i = 1; i <= m; i++)
    {
        cin >> x >> y;
        g.insert_edge(x, y, directed);
    }

    cout << g;
}
